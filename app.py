from flask import Flask, jsonify, request
import model


application = Flask(__name__)
mlmodel = model.MLModel()

@application.route('/', methods = ['GET'])
def index():
    return 'Тестовая страница из контейнера'

@application.route('/api', methods = ['POST'])
def make_prediction():
    content = request.get_json()
    try:
        data = content['data']
        prediction = mlmodel.predict(data)
    except:
        prediction = 'error. check your request'
    return jsonify({'predict': prediction})

if __name__ == '__main__':
    application.run(debug=True, host='0.0.0.0', port=5000)

# Пробный камент